<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMysqlQueryLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mysql_query_logs', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['select', 'insert', 'update', 'delete'])->default('select');
            $table->string('sql', '65535');
            $table->string('duration', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mysql_query_logs');
    }
}
