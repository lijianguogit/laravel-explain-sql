<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMysqlQueryExplain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mysql_query_explain', function (Blueprint $table) {
            $table->id();
            $table->integer('query_id');
            $table->tinyInteger('explain_id');
            $table->string('select_type');
            $table->string('table');
            $table->string('partitions')->nullable();
            $table->string('type');
            $table->string('possible_keys')->nullable();
            $table->string('key');
            $table->string('key_len');
            $table->string('ref')->nullable();
            $table->bigInteger('rows');
            $table->bigInteger('filtered');
            $table->string('extra')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mysql_query_explain');
    }
}
