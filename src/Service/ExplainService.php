<?php

namespace Jayli\ExplainQuery\Service;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

/**
 * Notes:
 * File name:explain
 * Create by: Jay.Li
 * Created on: 2021/9/10 0010 17:01
 */


class ExplainService
{
    public static function valid(string $sql):bool
    {
        $config = config('explain');

        if (Str::startsWith($sql, $config['ignore'])) {
            return true;
        }

        /**
         * from `users`
         * insert into `users`
         * update `users`
         * delete from `users`
         */

        $currentTables = array_map(function ($table) {
            return '`' . $table . '`';
        }, $config['ignoreTable']);

        $typeTables = [
            'from', 'insert into', 'update', 'delete'
        ];


        $result = [];
        array_map(function ($type) use ($currentTables, &$result) {

            foreach ($currentTables as $key => $table) {
                $result[] = sprintf('%s %s', $type, $table);
            }

        }, $typeTables);


        if (Str::contains($sql, $result)) {
            return true;
        }

        return false;
    }

    public static function handle(string $sql, string $time):void
    {
        if (!$sql) {
            goto end;
        }

        try {
            Log::info($sql);
            if (DB::table('mysql_query_logs')->where('sql', $sql)->count()) {
                goto end;
            }

            Log::debug($sql);
            $queryId = DB::table('mysql_query_logs')->insertGetId([
                'sql' => $sql,
                'duration' => $time,
                'type' => 'select'
            ]);

            if (!config('explain.openExplain')) {
                goto end;
            }

            $res = DB::select(sprintf('%s %s', 'explain', $sql));

            foreach ($res as $item) {
                $data = [
                    'query_id' => $queryId,
                    'explain_id' => $item->id,
                    "select_type" => $item->select_type,
                    "table" => $item->table,
                    "partitions" => $item->partitions ?? ' ',
                    "type" => $item->type,
                    "possible_keys" => $item->possible_keys ?? ' ',
                    "key" => $item->key ?? ' ',
                    "key_len" => $item->key_len ?? ' ',
                    "ref" => $item->ref ?? ' ',
                    "rows" => $item->rows ?? ' ',
                    "filtered" => $item->filtered ?? ' ',
                    "extra" => $item->Extra ?? ' ',
                ];

                DB::table('mysql_query_explain')->insert($data);
            }
        } catch (Throwable $e) {
            Log::error(sprintf('数据记录或者分析插入消息失败：' . $e->getMessage()));
        }

        end:
    }
}
