<?php

namespace Jayli\ExplainQuery\Provider;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Events\QueryExecuted;
use Jayli\ExplainQuery\Jobs\MysqlSelectExplain;
use Jayli\ExplainQuery\Service\ExplainService;

/**
 * Notes:
 * File name:
 * Create by: Jay.Li
 * Created on: 2021/9/10 0010 11:00
 */


class ServiceProvider extends LaravelServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/explain.php' => config_path('explain.php'),
        ], 'explain');

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations')
        ], 'migrations');

        if (!$this->app['config']->get('explain.open')) {
            goto end;
        }

        DB::listen(function (QueryExecuted $query) {
            $sqlWithPlaceholders = str_replace(['%', '?'], ['%%', '%s'], $query->sql);

            $bindings = $query->connection->prepareBindings($query->bindings);

            $pdo = $query->connection->getPdo();

            $sql = vsprintf($sqlWithPlaceholders, array_map([$pdo, 'quote'], $bindings));

            if (ExplainService::valid($sql)) {
                goto ignore;
            } else {
                $duration = $this->formatDuration($query->time / 1000);

                MysqlSelectExplain::dispatch($sql, $duration);
            }

            ignore:
            unset($sql);

        });

        end:
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }

    /**
     * Format duration.
     *
     * @param float $seconds
     *
     * @return string
     */
    private function formatDuration($seconds)
    {
        if ($seconds < 0.001) {
            return round($seconds * 1000000).'μs';
        } elseif ($seconds < 1) {
            return round($seconds * 1000, 2).'ms';
        }

        return round($seconds, 2).'s';
    }
}
