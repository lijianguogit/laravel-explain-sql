<?php
/**
 * Notes:
 * File name:MysqlSelectExplain
 * Create by: Jay.Li
 * Created on: 2021/9/10 0010 16:58
 */

namespace Jayli\ExplainQuery\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Jayli\ExplainQuery\Service\ExplainService;

class MysqlSelectExplain implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected string $sql;

    protected string $time;

    public int $tries = 5;

    public int $maxExceptions = 3;

    public int $timeout = 180;

    /**
     * Create a new job instance.
     *
     * @param string $sql
     * @param string $time
     */
    public function __construct(string $sql, string $time)
    {
        $config = config('explain.job');

        $this->queue = $config['queue'];
        $this->connection = $config['connection'];
        $this->tries = $config['tries'];
        $this->maxExceptions = $config['maxExceptions'];
        $this->timeout = $config['timeout'];

        $this->sql = $sql;

        $this->time = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ExplainService::handle($this->sql, $this->time);
    }
}
