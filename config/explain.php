<?php
/**
 * Notes:
 * File name:${fILE_NAME}
 * Create by: Jay.Li
 * Created on: 2021/9/10 0010 16:58
 */

return [
    /**
     * 不需要记录的类型
     * 'insert', 'update', 'delete', 'explain', 'show', 'create', 'alter'
     */
    'ignore' => ['insert', 'update', 'delete', 'explain', 'show', 'create', 'alter'],

    /**
     * 不需要记录的表
     */
    'ignoreTable' => [
        'mysql_query_logs',
        'mysql_query_explain'
    ],

    /**
     * 是否开启插件
     */
    'open' => true,

    /**
     * 是否开启explain分析语句
     */
    'openExplain' => true,

    /**
     * 队列的配置
     */
    'job' => [
        'queue' => 'mysql',
        'connection' => 'redis',
        'tries' => 5,
        'maxExceptions' => 3,
        'timeout' => 180,
    ]
];
