
* 安装
    ``composer require jayli/laravel-explain-sql``

* 特别说明
    * 当前扩展包最好是开发环境，不要在生产环境上使用

* 准备工作
    * 数据迁移 `php artisan vendor:publish --tag=migrations`, `php artisan migrate`
    * 配置文件发布 `php artisan vendor:publish --tag=explain`
    * 队列启动 `php artisan queue:work --queue=mysql --name=mysql`
        * 队列的配置在`explain.php`中，相关参考laravel队列
        * 队列的启动可以直接使用php命令行，node的pm2管理,[配置文件](./config/mysqlExplain.yml)
